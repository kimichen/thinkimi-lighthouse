import { graphql } from "gatsby"
import React from "react"
import ProductGrid from "../components/ProductGrid"
import SEO from "../components/SEO"

function ProductsPage(
  {
    data: {
      lh: {
        findAllProducts: allProducts,
      },
      allLighthouse: {
        nodes: [lighthouse],
      },
    },
  }) {
  return (
    <React.Fragment>
      <SEO pageTitle={lighthouse.products}/>
      <h1 className="font-bold text-3xl md:text-7xl mb-3 text-primary">
        {lighthouse.latest}
      </h1>

      <hr className="border-b border-gainsboro w-10"/>

      <ProductGrid products={allProducts}/>
    </React.Fragment>
  )
}

export const pageQuery = graphql`
query ProductsQuery ($locale: String!) {
   lh {
    findAllProducts(locale: $locale) {
      id
      name
      subProducts(locale: $locale) {
        id
        formattedPrice
        name
        retailPrice
        image
        imageFile {
          childImageSharp {
            fluid(maxWidth: 450) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
  allLighthouse(filter: {lng: {eq: $locale}}) {
    nodes {
      catalog
      products
      latest
      lng
    }
  }
}
`

export default ProductsPage
