import { graphql } from 'gatsby';
import React from 'react';
import Checkout from '../components/Checkout';
import SEO from '../components/SEO';


function CheckoutPage({
  data: {
    allLighthouse: {
      nodes: [lighthouse],
    }
  }
}) {
  return (
    <React.Fragment>
      <SEO pageTitle={lighthouse.checkout} />
      <div className="mb-6">
        <h1 className="font-bold text-3xl md:text-7xl mb-3 text-primary">
          {lighthouse.checkout}
        </h1>
        <hr className="border-b border-gainsboro w-10" />
      </div>
      <Checkout />
    </React.Fragment>
  );
}

export const pageQuery = graphql`
query CheckoutPageQuery($locale: String!) {
  allLighthouse(filter: {lng: {eq: $locale}}) {
    nodes {
      checkout
      lng
    }
  }
}
`

export default CheckoutPage;
