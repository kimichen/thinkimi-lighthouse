import React from "react"
import { graphql } from "gatsby"

import ProductGrid from "../components/ProductGrid"
import SEO from "../components/SEO"

function CollectionPage(
  {
    data: {
      lh: {
        findProductsBySlug: products,
        getSlug: collection,
      },
    },
  }) {
  return (
    <React.Fragment>
      <SEO pageTitle={collection.name}/>
      <h1 className="font-bold text-3xl md:text-7xl mb-3 text-primary">
        {collection.name}
      </h1>
      <hr className="border-b border-gainsboro w-10"/>

      <ProductGrid products={products}/>
    </React.Fragment>
  )
}

export const pageQuery = graphql`
query CollectionQuery($slug: String!, $locale: String!) {
  lh {
    getSlug(locale: $locale, slug: $slug){
      id
      name
      slug
    }
    findProductsBySlug(locale: $locale, slug: $slug) {
      id
      name
      slugs(locale: $locale) {
        id
        name
        slug
      }
      subProducts(locale: $locale) {
        id
        formattedPrice
        name
        retailPrice
        image
        imageFile {
          childImageSharp {
            fluid(maxWidth: 450) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
}
`

export default CollectionPage
