import { graphql } from 'gatsby';
import React from 'react';
import Cart from '../components/Cart';
import SEO from '../components/SEO';

function CartPage({
  data: {
    allLighthouse: {
      nodes: [lighthouse],
    }
  }
}) {

  return (
    <React.Fragment>
      <SEO pageTitle={lighthouse.cart} />
      <div className="mb-6">
        <h1 className="font-bold text-3xl md:text-7xl mb-3 text-primary">
          {lighthouse.cart}
        </h1>

        <hr className="border-b border-gainsboro w-10" />
      </div>

      <Cart />
    </React.Fragment>
  );
}

export const pageQuery = graphql`
query CartPageQuery($locale: String!) {
  allLighthouse(filter: {lng: {eq: $locale}}) {
    nodes {
      cart
      lng
    }
  }
}
`

export default CartPage;
