import { ClientContext } from "graphql-hooks"
import React, { useContext } from "react"

import { LocaleProvider } from "../context/Locale"

import "../styles/main.css"
import Footer from "./Footer"
import Header from "./Header"
import SEO from "./SEO"

function Layout({ children, location, pageContext: { locale } }) {

  const client = useContext(ClientContext)
  client.setHeader("Accept-Language", locale)

  return (
    <React.Fragment>
      <SEO/>
      <LocaleProvider locale={locale} location={location}>
        {/*<Banner />*/}
        <Header/>
        <div className="container mx-auto p-6 md:py-12 lg:py-16">
          {children}
        </div>
        <Footer/>
      </LocaleProvider>
    </React.Fragment>
  )
}

export default Layout
