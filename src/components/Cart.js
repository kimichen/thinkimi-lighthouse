import { graphql, useStaticQuery } from "gatsby"
import React, { useContext } from "react"
import { useCart } from "react-use-cart"
import LocaleContext from "../context/Locale"
import CartItemList from "./CartItemList"
import LocaleLink from "./LocaleLink"

const query = graphql`
query CartQuery {
  allLighthouse {
    nodes {
      subTotal
      label {
        checkout
      }
      cartEmpty
      lng
    }
  }
}
`

function Cart() {
  const {
    allLighthouse: {
      nodes: lighthouseNodes,
    }
  } = useStaticQuery(query)

  const { isEmpty, cartTotal, items } = useCart()

  const amount = items.reduce((accumulator, item) => accumulator + item.quantity, 0)

  const formattedSubTotal = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(cartTotal / 100)

  const { activeLocale } = useContext(LocaleContext)
  const lighthouse = lighthouseNodes.find(lighthouseNode => lighthouseNode.lng === activeLocale.toLowerCase())

  if (isEmpty) return <p>{lighthouse.cartEmpty}</p>

  return (
    <React.Fragment>

      <div className="mt-3 md:mt-6 py-3 md:py-6 ">
        <div className="flex flex-col items-end">
          <LocaleLink
            to="/checkout"
            className="bg-primary hover:bg-slategray px-4 py-3 rounded-lg text-white text-sm font-bold tracking-widest uppercase focus:outline-none"
          >
            {lighthouse.label.checkout}({amount})
          </LocaleLink>
        </div>
      </div>

      <CartItemList />

      <div className="mt-3 md:mt-6 py-3 md:py-6 border-t-2 border-gainsboro">
        <div className="flex flex-col items-end">
          <div className="flex flex-col items-end mb-3">
            <span className="text-slategray">{lighthouse.subTotal}</span>
            <span className="text-xl font-bold text-primary">
              {formattedSubTotal}
            </span>
          </div>

          <LocaleLink
            to="/checkout"
            className="bg-primary hover:bg-slategray px-4 py-3 rounded-lg text-white text-sm font-bold tracking-widest uppercase focus:outline-none"
          >
            {lighthouse.label.checkout}({amount})
          </LocaleLink>
        </div>
      </div>


    </React.Fragment>
  )
}

export default Cart
