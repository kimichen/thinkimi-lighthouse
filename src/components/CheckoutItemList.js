import { graphql, useStaticQuery } from 'gatsby';
import React, { useContext } from 'react';
import { useCart } from 'react-use-cart';
import CheckoutContext from '../context/Checkout';
import LocaleContext from "../context/Locale";
import CheckoutItem from './CheckoutItem';

const query = graphql`
query CheckoutItemListQuery {
  allLighthouse {
    nodes {
      subTotal
      tax
      shipping
      total
      lng
    }
  }
}
`

function CheckoutItemList() {
  const {
    allLighthouse: {
      nodes: lighthouseNodes,
    }
  } = useStaticQuery(query)

  const { activeLocale } = useContext(LocaleContext)
  const lighthouse = lighthouseNodes.find(lighthouseNode => lighthouseNode.lng === activeLocale.toLowerCase())

  const { items, cartTotal } = useCart();
  const { orderTotal, shippingFee, tax } = useContext(CheckoutContext);

  const formatValue = value =>
    new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
    }).format(value / 100);

  return (
    <div className="rounded bg-white border-2 border-gainsboro p-3 md:p-6 my-3 md:my-6">
      {items.map(CheckoutItem)}
      <div className="flex items-center justify-between">
        <span className="text-sm text-slategray">{lighthouse.subTotal}</span>
        <span className="font-semibold">{formatValue(cartTotal)}</span>
      </div>
      <div className="flex items-center justify-between">
        <span className="text-sm text-slategray">{lighthouse.tax}</span>
        <span className="font-semibold">{tax ? formatValue(tax) : '-'}</span>
      </div>
      <div className="flex items-center justify-between">
        <span className="text-sm text-slategray">{lighthouse.shipping}</span>
        <span className="font-semibold">
          {shippingFee ? formatValue(shippingFee) : '-'}
        </span>
      </div>
      <hr className="border-b border-gainsboro my-3 w-20" />
      <div className="flex items-center justify-between">
        <span className="text-lg font-semibold text-slategray">{lighthouse.total}</span>
        <span className="text-xl font-bold text-primary">
          {formatValue(orderTotal)}
        </span>
      </div>
    </div>
  );
}

export default CheckoutItemList;
