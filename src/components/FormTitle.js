import React from "react";

const FormTitle = ({ name }) =>
  <h3 className="text-slategray text-2xl md:text-3xl font-bold mb-6">
    {name}
  </h3>

export { FormTitle };
