import { graphql, navigate, useStaticQuery } from 'gatsby';
import React, { useContext, useEffect } from 'react';
import { useCart } from 'react-use-cart';
import CheckoutForm from '../components/CheckoutForm';
import { CheckoutProvider } from '../context/Checkout';
import LocaleContext from "../context/Locale";
import CheckoutItemList from './CheckoutItemList';

const query = graphql`
query CheckoutQuery {
  allLighthouse {
    nodes {
      cartEmpty
      lng
    }
  }
}
`

function Checkout() {
  const {
    allLighthouse: {
      nodes: lighthouseNodes,
    }
  } = useStaticQuery(query)

  const { activeLocale } = useContext(LocaleContext)
  const lighthouse = lighthouseNodes.find(lighthouseNode => lighthouseNode.lng === activeLocale.toLowerCase())
  const { cartEmpty } = lighthouse


  const { isEmpty } = useCart();

  useEffect(() => {
    if (isEmpty) {
      const navigateTimer = setTimeout(() => {
        navigate(`/cart`);
      }, 3000);

      return () => clearTimeout(navigateTimer);
    }
  }, [isEmpty]);

  if (isEmpty) return <p>{cartEmpty}</p>;

  return (
    <CheckoutProvider>
      <div className="lg:flex -mx-4">
        <div className="lg:w-1/2 lg:w-2/5 px-4 order-last">
          <div className="lg:sticky lg:top-0">
            <CheckoutItemList />
          </div>
        </div>
        <div className="lg:w-1/2 lg:w-3/5 px-4 order-first">
          <CheckoutForm />
        </div>
      </div>
    </CheckoutProvider>
  );
}

export default Checkout;
