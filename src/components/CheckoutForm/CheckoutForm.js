// import { useMutation } from 'graphql-hooks';
import { navigate } from "gatsby"
import React, { useContext } from "react"
import { FormContext, useForm } from "react-hook-form"
import { toast } from "react-toastify"
import { useCart } from "react-use-cart"
import CheckoutContext from "../../context/Checkout"
import BillingForm from "./BillingForm"
import InfoForm from "./InfoForm"
import ShippingForm from "./ShippingForm"


// const CALCULATE_MUTATION = `mutation estimateOrderCosts,($input: EstimateOrderCostsInput!) {
//   estimateOrderCosts,(input: $input) {
//     currency
//     shippingRate
//     taxRate
//     vatRate
//   }
// }`;
//
// const CHECKOUT_MUTATION = `mutation checkout($input: CheckoutInput!) {
//   checkout(input: $input) {
//     graphCMSOrderId
//     printfulOrderId
//   }
// }`;
//
// const PAYMENT_INTENT_MUTATION = `mutation createPaymentIntent($input: PaymentIntentInput!) {
//   createPaymentIntent(input: $input) {
//     id
//     clientSecret
//     status
//   }
// }`;

function CheckoutForm() {
  const methods = useForm({
    defaultValues: {
      separateBilling: false,
      needShipping: false,
      billing: {
        country: "CN",
      },
      shipping: {
        country: "CN",
      },
    },
  })
  const { handleSubmit, watch } = methods
  // const [estimateOrderCosts] = useMutation(CALCULATE_MUTATION);
  // const [checkout] = useMutation(CHECKOUT_MUTATION);
  // const [createPaymentIntent] = useMutation(PAYMENT_INTENT_MUTATION);
  const { emptyCart, items } = useCart()
  const { needShipping, separateBilling, firstName, lastName } = watch()
  const {
    allowShipping,
    allowPayment,
    checkoutPayment,
    checkoutError,
    checkoutProcessing,
    checkoutSuccess,
    orderTotal,
    updateShipping,
    updateTax,
  } = useContext(CheckoutContext)
  // const stripe = useStripe()
  // const elements = useElements()

  const useSeparateBilling = !!separateBilling
  const useShipping = !!needShipping
  const name = ((lastName || "").toUpperCase() + " " + (firstName || "").toUpperCase()).trim();

  const handleCheckoutError =
    ({
      message = "Unable to process order. Please try again",
    }) => {
      checkoutError({ message })

      toast.error(message, {
        className: "bg-red",
      })
    }

  const handleCheckoutSuccess = orderId => {
    checkoutSuccess()

    emptyCart()

    navigate("success", { state: { orderId } })
  }

  const validateOrder = async values => {
    checkoutProcessing()

    try {

      const fee = items.reduce((accumulator, item) => {
        return {
          taxTotal: parseInt(accumulator.taxTotal) + Math.round((parseInt(item.tax) + parseInt(item.vat)) * item.price),
          shippingTotal: Math.max(accumulator.shippingTotal, item.shipping)
        }
      }, {
        taxTotal: 0,
        shippingTotal: 0
      })

      updateTax(Math.round(fee.taxTotal / 100))
      updateShipping(fee.shippingTotal)

      checkoutPayment()
    } catch (err) {
      handleCheckoutError(err)
    }
  }

  const submitOrder = async values => {
    checkoutProcessing()

    try {
      const {
        firstName,
        lastName,
        email,
        phone,
        shipping: { name, ...rest },
        billing: billingAddress,
      } = values

      const checkoutItems = items.map(
        ({ id: variantId, description, image, ...rest }) => ({
          variantId,
          ...rest,
        }),
      )

      const shippingAddress = { name, ...rest }

      const input = {
        firstName,
        lastName,
        email,
        phone,
        total: orderTotal,
        shippingAddress,
        billingAddress: useSeparateBilling ? billingAddress : shippingAddress,
        items: checkoutItems,
      }

      // const {
      //   data: {
      //     checkout: { graphCMSOrderId, printfulOrderId },
      //   },
      // } = await checkout({
      //   variables: {
      //     input,
      //   },
      // });

      const graphCMSOrderId = new Date().getMilliseconds()
      const printfulOrderId = new Date().getMilliseconds()

      // const {
      //   data: {
      //     createPaymentIntent: { clientSecret },
      //   },
      // } = await createPaymentIntent({
      //   variables: {
      //     input: {
      //       description: `SWAG store Printful order ${printfulOrderId}`,
      //       email,
      //       metadata: { graphCMSOrderId, printfulOrderId },
      //       total: orderTotal,
      //     },
      //   },
      // })

      const clientSecret = "sssss"

      // const { error } = await stripe.confirmCardPayment(clientSecret, {
      //   payment_method: {
      //     card: elements.getElement("card"),
      //   },
      // })

      const error = false

      if (error) throw new Error(error.message)

      handleCheckoutSuccess(graphCMSOrderId)
    } catch (err) {
      handleCheckoutError(err)
    }
  }

  return (
    <FormContext {...methods}>
      <form
        onSubmit={handleSubmit(
          allowPayment ? submitOrder : validateOrder,
        )}
      >
        <InfoForm />
        {useShipping && <ShippingForm />}
        {useShipping && useSeparateBilling && <BillingForm />}
        {/* <PaymentForm /> */}
      </form>
    </FormContext>
  )
}

export default CheckoutForm
