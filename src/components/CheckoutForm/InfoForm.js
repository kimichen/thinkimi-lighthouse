import classnames from 'classnames';
import { graphql, useStaticQuery } from 'gatsby';
import React, { useContext } from 'react';
import { useFormContext } from 'react-hook-form';
import CheckoutContext from '../../context/Checkout';
import LocaleContext from "../../context/Locale";
import LoadingSVG from '../../svg/loading.svg';
import Checkbox from '../Checkbox';
import { FormTitle } from '../FormTitle';
import Input from '../Input';

const query = graphql`
query InfoFormQuery {
  allLighthouse {
    nodes {
      message {
        firstNameRequire
        lastNameRequire
        emailRequire
        emailInvalid
      }
      placeholder {
        firstName
        lastName
        email
      }
      checkbox {
        needShipping
      }
      form {
        infoForm {
          name
        }
      }
      label {
        order
      }
      lng
    }
  }
}
`

function InfoForm() {

  const {
    allLighthouse: {
      nodes: lighthouseNodes,
    }
  } = useStaticQuery(query)

  const { activeLocale } = useContext(LocaleContext)
  const lighthouse = lighthouseNodes.find(lighthouseNode => lighthouseNode.lng === activeLocale.toLowerCase())
  const { placeholder, message, checkbox, label, form: { infoForm } } = lighthouse;

  const { errors, register, watch } = useFormContext();
  const { allowPayment, processing: checkoutProcessing } = useContext(
    CheckoutContext
  );

  const { needShipping } = watch();
  const useShipping = !!needShipping


  const disableInput = allowPayment || checkoutProcessing;

  return (
    <div className="rounded-lg bg-white border-2 border-gainsboro p-3 md:p-6 my-3 md:my-6">
      <FormTitle name={infoForm.name} />

      <div className="md:flex -mx-3">

        <div className="md:w-1/2 mb-3 md:mb-6 px-3">
          <Input
            name="lastName"
            placeholder={placeholder.lastName}
            disabled={disableInput}
            register={register({ required: message.lastNameRequire })}
            errors={errors}
          />
        </div>

        <div className="md:w-1/2 mb-3 md:mb-6 px-3">
          <Input
            name="firstName"
            placeholder={placeholder.firstName}
            disabled={disableInput}
            register={register({ required: message.firstNameRequire })}
            errors={errors}
          />
        </div>

      </div>

      <div className="mb-3 md:mb-6">
        <Input
          name="email"
          type="email"
          placeholder={placeholder.email}
          disabled={disableInput}
          register={register({
            required: message.emailRequire,
            pattern: {
              value: /^\S+@\S+$/i,
              message: message.emailInvalid,
            },
          })}
          errors={errors}
        />
      </div>

      {!allowPayment && (
        <div className="flex items-center justify-between">
          <Checkbox
            name="needShipping"
            disabled={disableInput}
            register={register}
          >
            {checkbox.needShipping}
          </Checkbox>
          {!useShipping && (
            <button
              type="submit"
              className={classnames(
                'bg-primary rounded-lg text-white px-3 py-2 h-10 focus:outline-none font-bold',
                { 'cursor-not-allowed opacity-50': disableInput }
              )}
              disabled={disableInput}
            >
              {checkoutProcessing ? <LoadingSVG /> : label.order}
            </button>
          )}
        </div>
      )}
    </div>
  );

}

export default InfoForm;
