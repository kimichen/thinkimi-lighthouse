import classnames from 'classnames';
import { graphql, useStaticQuery } from 'gatsby';
import React, { useContext } from 'react';
import { useFormContext } from 'react-hook-form';
import CheckoutContext from '../../context/Checkout';
import LocaleContext from "../../context/Locale";
import LoadingSVG from '../../svg/loading.svg';
import Checkbox from '../Checkbox';
import { FormTitle } from '../FormTitle';
import Input from '../Input';
import Select from '../Select';



const query = graphql`
query ShippingFormQuery {
  allLighthouse {
    nodes {
      message {
        nameRequire 
        emailRequire
        phoneRequire
        address1Require
        cityRequire
        countryRequire
        stateRequire
        zipRequire
      }
      placeholder {
        name
        phone
        email
        address1
        address2
        city
        country
        state
        zip
      }
      label {
        order
      }
      checkbox {
        separateBilling
      }
      form {
        shippingForm {
          name
        }
      }
      lng
    }
  }
  allCountry {
    nodes {
      lng
      name
      code
      states {
        code
        name
      }
    }
  }
}
`

function ShippingForm() {
  const {
    allLighthouse: {
      nodes: lighthouseNodes,
    },
    allCountry: {
      nodes: countryNodes
    }
  } = useStaticQuery(query)

  const { activeLocale } = useContext(LocaleContext)
  const lighthouse = lighthouseNodes.find(lighthouseNode => lighthouseNode.lng === activeLocale.toLowerCase())
  const countries = countryNodes.filter(country => country.lng === activeLocale.toLowerCase())
  const { placeholder, message, label, checkbox, form: { shippingForm } } = lighthouse;


  const { errors, register, watch } = useFormContext();

  const { allowPayment, processing: checkoutProcessing } = useContext(CheckoutContext);

  const { separateBilling, shipping: { country: shippingCountryCode } = {} } = watch({ nest: true });

  const useSeparateBilling = !!separateBilling

  const activeShippingCountry = countries.find(
    country => country.code === shippingCountryCode
  );

  const disableInput = allowPayment || checkoutProcessing;

  return (
    <div className="rounded-lg bg-white border-2 border-gainsboro p-3 md:p-6 my-3 md:my-6">

      <FormTitle name={shippingForm.name} />

      <div className="md:flex -mx-3">

        <div className="md:w-1/2 mb-3 md:mb-6 px-3">
          <Input
            name="shipping.name"
            placeholder={placeholder.name}
            disabled={disableInput}
            register={register({ required: message.nameRequire })}
            errors={errors}
          />
        </div>

        <div className="md:w-1/2 mb-3 md:mb-6 px-3">
          <Input
            name="phone"
            type="tel"
            placeholder={placeholder.phone}
            disabled={disableInput}
            register={register({ required: message.phoneRequire })}
            errors={errors}
          />
        </div>
      </div>

      <div className="mb-3 md:mb-6">
        <Input
          name="shipping.address1"
          placeholder={placeholder.address1}
          disabled={disableInput}
          register={register({
            required: message.address1Require,
          })}
          errors={errors}
        />
      </div>

      <div className="mb-3 md:mb-6">
        <Input
          name="shipping.address2"
          placeholder={placeholder.address2}
          disabled={disableInput}
          register={register}
          errors={errors}
        />
      </div>

      <div className="md:flex -mx-3">
        <div className="md:w-1/2 mb-3 md:mb-6 px-3">
          <Input
            name="shipping.city"
            placeholder={placeholder.city}
            disabled={disableInput}
            register={register({ required: message.cityRequire })}
            errors={errors}
          />
        </div>

        <div className="md:w-1/2 mb-3 md:mb-6 px-3">
          <Select
            name="shipping.country"
            disabled={disableInput}
            register={register({ required: message.countryRequire })}
            options={countries.map(({ code: value, name }) => ({
              value,
              name,
            }))}
            errors={errors}
          />
        </div>
      </div>

      <div className="md:flex -mx-3">
        {activeShippingCountry && activeShippingCountry.states && (
          <div className="md:w-1/2 mb-3 md:mb-6 px-3">
            <Select
              name="shipping.state"
              disabled={disableInput}
              register={register({ required: message.stateRequire })}
              options={activeShippingCountry.states.map(
                ({ code: value, name }) => ({
                  value,
                  name,
                })
              )}
              errors={errors}
            />
          </div>
        )}

        <div className="md:w-1/2 mb-3 md:mb-6 px-3">
          <Input
            name="shipping.zip"
            placeholder={placeholder.zip}
            disabled={disableInput}
            register={register({ required: message.zipRequire })}
            errors={errors}
          />
        </div>
      </div>

      {!allowPayment && (
        <div className="flex items-center justify-between">
          <Checkbox
            name="separateBilling"
            disabled={disableInput}
            register={register}
          >
            {checkbox.separateBilling}
          </Checkbox>
          {!useSeparateBilling && (<button
            type="submit"
            className={classnames(
              'bg-primary rounded-lg text-white px-3 py-2 h-10 focus:outline-none font-bold',
              { 'cursor-not-allowed opacity-50': disableInput }
            )}
            disabled={disableInput}
          >
            {checkoutProcessing ? <LoadingSVG /> : label.order}
          </button>)}
        </div>
      )}
    </div>
  );
}

export default ShippingForm;
