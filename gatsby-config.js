require(`dotenv`).config()

module.exports = {
  siteMetadata: {
    title: `Lighthouse`,
    description: `All your needs on lighthouse.`,
    siteUrl: `https://lighthousecuba.com`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `thinkimi-lighthouse`,
        short_name: `lt`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/icon.png`,
      },
    },
    `gatsby-plugin-postcss`,
    {
      resolve: "gatsby-plugin-purgecss",
      options: {
        tailwind: true,
        purgeOnly: ["src/styles/main.css"],
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /svg/,
        },
      },
    },
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/locale`,
        name: `locale`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-env-variables`,
      options: {
        whitelist: ["GRAPHLH_ENDPOINT"],
      },
    },
    {
      resolve: `gatsby-source-graphql`,
      options: {
        typeName: `GraphLH`,
        fieldName: `lh`,
        url: process.env.GRAPHLH_ENDPOINT,
        headers: {
          // Authorization: `Bearer ${process.env.GRAPHLH_QUERY_TOKEN}`,
          "Accept-Language": JSON.parse(
            typeof window !== "undefined" && window.localStorage.getItem("lighthouse-store"),
          ).activeLocale || "en",
        },
      },
    },
  ],
}
