import * as axios from "axios"
import { ClientContext, GraphQLClient } from "graphql-hooks"

import React from "react"
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import { CartProvider } from "react-use-cart"

import Layout from "./src/components/Layout"
import StripeProvider from "./src/components/StripeProvider"
import { handleItemAdded, handleItemRemoved, handleItemUpdated } from "./src/utils/cart-helpers"


const toastOptions = {
  position: "top-right",
  draggable: false,
  toastClassName:
    "bg-primary text-white text-center px-2 py-3 shadow-none rounded-lg",
  progressClassName: "h-0",
  closeButton: false,
  autoClose: 2000,
}

const config = {
  url: process.env.GRAPHLH_ENDPOINT,
  fetch: axios
}

const client = new GraphQLClient(config)

const randomCartId = () =>
  Math.random()
    .toString(36)
    .substring(7)

export const wrapPageElement = ({ element, props }) => {
  return <Layout {...props}>{element}</Layout>
}

export const wrapRootElement = ({ element }) => {
  return (
    <ClientContext.Provider value={client}>
      <StripeProvider>
        <CartProvider
          id={randomCartId()}
          onItemAdd={handleItemAdded}
          onItemUpdate={handleItemUpdated}
          onItemRemove={handleItemRemoved}
        >
          {element}
        </CartProvider>
        <ToastContainer {...toastOptions} />
      </StripeProvider>
    </ClientContext.Provider>
  )
}
